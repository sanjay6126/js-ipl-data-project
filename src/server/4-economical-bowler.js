

  function tenEcnomicalBowlersOf2015(matches,deliveries){
    let matchIds2015 = matches.filter((match) => match.season == "2015").map((match) => match.id)


    const bowlerStats = deliveries.reduce((ballsRuns,currentDelivery) => {
        if(matchIds2015.includes(currentDelivery.match_id)){
            
          if(ballsRuns.hasOwnProperty(currentDelivery.bowler)){
            ballsRuns[currentDelivery.bowler]["runs"] +=  Number(currentDelivery.total_runs); 
            ballsRuns[currentDelivery.bowler]["balls"] += 1; 
          }
          else{

            ballsRuns[currentDelivery.bowler] = {};
            ballsRuns[currentDelivery.bowler]["runs"] = Number(currentDelivery.total_runs);
            ballsRuns[currentDelivery.bowler]["balls"] = 1;                                 
          }
        }

        return ballsRuns;
    },{});


    const bowlersEconomy = Object.entries(bowlerStats).reduce((acc,current) => {
      
      let economy = current[1].runs / current[1].balls;
      acc[current[0]] = economy;
      
      return acc;
    },{});



    let result = Object.entries(bowlersEconomy).sort((a,b) => a[1]-b[1]).slice(0,11)
    
    return Object.fromEntries(result);


  }








module.exports = tenEcnomicalBowlersOf2015;