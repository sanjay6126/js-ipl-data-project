
function tossWinnerMatchWinner(matches) {
  
    let result = matches.reduce((tossMatch, match) => {
      if (tossMatch.hasOwnProperty(match.season)) {
        if (match.toss_winner == match.winner) {
          tossMatch[match.season] += 1;
        }
      }else{
        if (match.toss_winner == match.winner) {
        tossMatch[match.season] = 1;            
        }
      }
      return tossMatch;
    }, {});
  
    return result;
}


module.exports = tossWinnerMatchWinner;