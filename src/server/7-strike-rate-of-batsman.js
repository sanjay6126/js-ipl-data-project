
function strikeRateOfBatsman(deliveries){

    const res = deliveries.reduce((batsmanStats,currentDelivery) => {
        if(batsmanStats.hasOwnProperty(currentDelivery.batsman)){
            batsmanStats[currentDelivery.batsman]["batsmanRun"] += Number(currentDelivery.batsman_runs); 
            batsmanStats[currentDelivery.batsman]["ballsFaced"] += 1; 
        }
        else{
            batsmanStats[currentDelivery.batsman] = {};
            batsmanStats[currentDelivery.batsman]["batsmanRun"] = Number(currentDelivery.batsman_runs);
            batsmanStats[currentDelivery.batsman]["ballsFaced"] = 1;
        }

        return batsmanStats;
    },{});

    const result = Object.entries(res).reduce((acc,curr) => {
        let strike_rate = (curr[1].batsmanRun / curr[1].ballsFaced)*100;
        acc[curr[0]] = strike_rate;
    
        return acc;
    },{});

    return result;

}




module.exports = strikeRateOfBatsman;


