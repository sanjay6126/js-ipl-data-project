
function bestEconomyInSuperOvers(deliveries,matches){

    const matchesTied = matches.filter((match) => match.result === "tie")
    const matchIds = matchesTied.map((match) => match.id);
    

    
    const bowlerStats = deliveries.reduce((ballsRuns,currentDelivery) => {
        if(matchIds.includes(currentDelivery.match_id)){
            
          if(ballsRuns.hasOwnProperty(currentDelivery.bowler)){
            ballsRuns[currentDelivery.bowler]["runs"] +=  Number(currentDelivery.total_runs); 
            ballsRuns[currentDelivery.bowler]["balls"] += 1; 
          }
          else{

            ballsRuns[currentDelivery.bowler] = {};
            ballsRuns[currentDelivery.bowler]["runs"] = Number(currentDelivery.total_runs);
            ballsRuns[currentDelivery.bowler]["balls"] = 1;                                 
          }
        }

        return ballsRuns;
    },{});



    const bowlersEconomyInSuperOver = Object.entries(bowlerStats).reduce((acc,current) => {
      
        let economy = current[1].runs / ((current[1].balls)/6);
        acc[current[0]] = economy;
        
        return acc;
    },{});
  
    

    let result = Object.entries(bowlersEconomyInSuperOver).sort((a,b) => a[1]-b[1]).slice(0,1)
    
    return Object.fromEntries(result);



}



module.exports = bestEconomyInSuperOvers;

