
  function playerOfTheMatch(matches){
  
    let manOfTheMatches = matches.reduce((manOfTheMatches, match) => {
      if (manOfTheMatches.hasOwnProperty(match.season)) {
        if (manOfTheMatches[match.season].hasOwnProperty(match.player_of_match)) {
            manOfTheMatches[match.season][match.player_of_match] += 1;                  //{'2008': {'xy':1}, }   //mom[2008][xy]
        } 
        else {
            manOfTheMatches[match.season][match.player_of_match] = 1;
        }
            
      }
      else {  
        manOfTheMatches[match.season] = {};               //{'2008':{}}
        manOfTheMatches[match.season][match.player_of_match] = 1;
      }

      return manOfTheMatches;

    }, {});

    return manOfTheMatches;


  }



module.exports = playerOfTheMatch;