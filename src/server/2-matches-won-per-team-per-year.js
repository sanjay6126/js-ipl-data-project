
function matchesWonPerTeamPerYear(matches) {
    
    let Result = matches.reduce((matchResult, currentMatch) => {
      if (matchResult.hasOwnProperty(currentMatch.season)) {        
        if (matchResult[currentMatch.season].hasOwnProperty(currentMatch.winner)) {
          matchResult[currentMatch.season][currentMatch.winner] += 1;
        } 
        else {
            if (currentMatch.winner != ''){
              matchResult[currentMatch.season][currentMatch.winner] = 1;
            }
          }
      }
      else {
        matchResult[currentMatch.season] = {};                          
      }
      return matchResult;
    }, {});
  
    return Result;
}
  



module.exports = matchesWonPerTeamPerYear;



