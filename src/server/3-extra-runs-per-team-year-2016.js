


function extraRunsPerTeamYear2016(matches,deliveries){
    let matchIds = matches.filter((match) => match.season == "2016").map((match) => match.id)


    let result = deliveries.reduce((totalExtraRun,currentDelivery) => {
        if(matchIds.includes(currentDelivery.match_id)){
            if(totalExtraRun.hasOwnProperty(currentDelivery.bowling_team)){
                totalExtraRun[currentDelivery.bowling_team] += parseInt(currentDelivery.extra_runs); 
            }
            else{
                totalExtraRun[currentDelivery.bowling_team] = parseInt(currentDelivery.extra_runs);
            }
        }

        return totalExtraRun;
    },{});

    return result;

}



module.exports = extraRunsPerTeamYear2016;