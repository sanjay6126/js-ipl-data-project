

// const fs = require('fs');
// const csv = require('csvtojson');
// const path = require('path');

// const MATCHES_FILE_PATH = path.resolve(__dirname,'../data/matches.csv')

// csv().fromFile(MATCHES_FILE_PATH).then((matches) => {
//     let result = matchesPlayedPerYear(matches);
//     fs.writeFileSync('../public/output/1-matches-per-year.json',JSON.stringify(result));
// })


function matchesPlayedPerYear(matches){

    if (matches === undefined || !Array.isArray(matches)) {
        return {};
    }



    const result = matches.reduce(function(acc,curr){
        if(acc[curr.season]){                   
            acc[curr.season] += 1;
        }
        else{
            acc[curr.season] = 1;       
        }

        return acc;

    },{})
    return result;
}

module.exports = matchesPlayedPerYear;
