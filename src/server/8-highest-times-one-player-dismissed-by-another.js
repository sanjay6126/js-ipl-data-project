
function highestTimePlayerDismissedByAnother(deliveries){

    const batterOutToDifferentBowler = deliveries.reduce((batterStats,currentDelivery) => {
        if(currentDelivery.dismissal_kind !=="" && currentDelivery.dismissal_kind !== "run out"){
            if(batterStats.hasOwnProperty(currentDelivery.batsman)){
                if(batterStats[currentDelivery.batsman].hasOwnProperty(currentDelivery.bowler)){
                    batterStats[currentDelivery.batsman][currentDelivery.bowler] += 1;
                }
                else{
                    batterStats[currentDelivery.batsman][currentDelivery.bowler] = 1;
                }
            }
            else{
                batterStats[currentDelivery.batsman] = {};
                batterStats[currentDelivery.batsman][currentDelivery.bowler]=1;
            }
        }

        return batterStats;
    },{});



    let result = Object.entries(batterOutToDifferentBowler).filter((playerInfo) => {
        let bowlerName = Object.entries(playerInfo[1]).sort((a,b) => b[1]-a[1]).slice(0,1);
        
        playerInfo[1] = Object.fromEntries(bowlerName);

        return playerInfo;

    });



    return result;



}


module.exports = highestTimePlayerDismissedByAnother;
