
const fs = require('fs');
const csv = require('csvtojson');
const path = require('path');

const MATCHES_FILE_PATH = path.join(__dirname, "./src/data/matches.csv");
const DELIVERIES_FILE_PATH = path.join(__dirname, "./src/data/deliveries.csv");
const OUTPUT_DIRECTORY = path.join(__dirname, './src/public/output/');

const matchesPlayedPerYear = require('./src/server/1-matches-per-year');
const matchesWonPerTeamPerYear = require('./src/server/2-matches-won-per-team-per-year');
const extraRunConcededByEachTeam = require('./src/server/3-extra-runs-per-team-year-2016');
const topTenEconomicalBowlersByYear = require('./src/server/4-economical-bowler');
const tossWinMatchWin = require('./src/server/5-toss-winner-match-winner');
const highestPlayerOfMatchesWinner = require('./src/server/6-player-of-the-match');
const strikeRateOfBatsmanPerYear = require('./src/server/7-strike-rate-of-batsman');
const highestPlayerDismissed = require('./src/server/8-highest-times-one-player-dismissed-by-another');
const bestEconomicBowlerInSuperOver = require('./src/server/9-best-economy-in-super-over');


function main() {
  csv()
    .fromFile(MATCHES_FILE_PATH)
    .then(matches => {
      csv()
        .fromFile(DELIVERIES_FILE_PATH)
        .then(deliveries => {



            let totalMatchesPlayedPerYear = JSON.stringify(matchesPlayedPerYear(matches));
            fs.writeFile(path.join(OUTPUT_DIRECTORY, '1-matches-per-year.json'),totalMatchesPlayedPerYear,(error) => {

                if (error) {
                    console.error(error);
                }
            });

            let totalMatchesWonPerTeamPerYear = JSON.stringify(matchesWonPerTeamPerYear(matches));
            fs.writeFile(path.join(OUTPUT_DIRECTORY, '2-matches-won-per-team-per-year.json'), totalMatchesWonPerTeamPerYear, (error) => {
                if (error) {
                console.error(error);
                }
            });

            let extraRunsConcededPerTeam = JSON.stringify(extraRunConcededByEachTeam(matches, deliveries));

            fs.writeFile(path.join(OUTPUT_DIRECTORY, `3-extra-runs-per-team-year-2016.json`), extraRunsConcededPerTeam, (error) => {
                if (error) {
                console.error(error);
                }
            });

            let topTenEconomicBowlers = JSON.stringify(topTenEconomicalBowlersByYear(matches, deliveries));   

            fs.writeFile(path.join(OUTPUT_DIRECTORY, `4-economical-bowler.json`), topTenEconomicBowlers, (error) => {
                if (error) {
                console.error(error);
                }
            });

            let totalTossWinMatchWin = JSON.stringify(tossWinMatchWin(matches));   

            fs.writeFile(path.join(OUTPUT_DIRECTORY, `5-toss-winner-match-winner.json`), totalTossWinMatchWin, (error) => {
                if (error) {
                console.error(error);
                }
            });

            let totalPlayerOfMatchesWinner = JSON.stringify(highestPlayerOfMatchesWinner(matches));   

            fs.writeFile(path.join(OUTPUT_DIRECTORY, `6-player-of-the-match.json`), totalPlayerOfMatchesWinner, (error) => {
                if (error) {
                console.error(error);
                }
            });


            let totalStrikeRateOfBatsmanPerYear = JSON.stringify(strikeRateOfBatsmanPerYear(deliveries));   

            fs.writeFile(path.join(OUTPUT_DIRECTORY, `7-strike-rate-of-batsman.json`), totalStrikeRateOfBatsmanPerYear, (error) => {
                if (error) {
                console.error(error);
                }
            });


            let totalHighestPlayerDismissed = JSON.stringify(highestPlayerDismissed(deliveries));   

            fs.writeFile(path.join(OUTPUT_DIRECTORY, `8-highest-times-one-player-dismissed-by-another.json`), totalHighestPlayerDismissed, (error) => {
                if (error) {
                console.error(error);
                }
            });


            let bestBowlerInSuperOver = JSON.stringify(bestEconomicBowlerInSuperOver(deliveries,matches));   

            fs.writeFile(path.join(OUTPUT_DIRECTORY, `9-best-economy-in-super-over.json`), bestBowlerInSuperOver, (error) => {
                if (error) {
                console.error(error);
                }
            });


            
        })
    })
}

main();








